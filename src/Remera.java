
public class Remera extends Prenda{

	public Remera(Double precioDeLista) {
		super(precioDeLista); 
	}

	@Override
	public Double calcularPrecio() {
		return (this.getPrecioDeLista() + 100) * 1.10;
	}
}
