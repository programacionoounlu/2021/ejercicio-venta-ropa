import java.util.ArrayList;
import java.util.List;

public class Test {
	public static void main(String args[]) {
		Prenda prenda = new Prenda(100.00);
		System.out.println(prenda.getPrecioDeLista() + " debería ser $110.00");
		Camisa camisaMangaCorta = new Camisa(100.00, false);
		System.out.println(camisaMangaCorta.calcularPrecio() + " debería ser $110.00");
		Camisa camisaMangaLarga = new Camisa(100.00, true);
		System.out.println(camisaMangaLarga.calcularPrecio() + " debería ser $115.00");
		Remera remera = new Remera(100.00);
		System.out.println(remera.calcularPrecio() + " debería ser $220.00");
		Sweater sweater = new Sweater(100.00);
		System.out.println(sweater.calcularPrecio() + " debería ser $108.00");
		
		Prenda a = new Remera(100.00);
		Prenda b = new Sweater(100.00);
		Prenda c = new Prenda(100.00);
		Prenda d = new Camisa(100.00, true);
		//System.out.println(a.calcularPrecio() + " debería ser $220.00");
		//System.out.println(b.calcularPrecio() + " debería ser $108.00");
		//System.out.println(c.calcularPrecio() + " debería ser $110.0");
		if(d instanceof Camisa) {
			Camisa x = (Camisa) d;
			//System.out.println("d es una camisa");
		}
		List<Prenda> prendas = new ArrayList<>();
		prendas.add(camisaMangaCorta);
		prendas.add(camisaMangaLarga);
		prendas.add(remera);
		prendas.add(sweater);
		
		for (Prenda prenda2 : prendas) {
			System.out.print("El precio de la prenda es de $ " + prenda2.calcularPrecio());
			if(prenda2 instanceof Camisa) {
				Camisa k = (Camisa) prenda2;
				if(k.isMangaLarga()) {
					System.out.println(" y es manga larga");
				}else {
					System.out.println(" y no es manga larga");
				}
			}
		}
	}
}
