
public class Prenda {
	private Double precioDeLista;

	public Prenda(Double precioDeLista) {		
		this.setPrecioDeLista(precioDeLista);
	}
	
	public Double calcularPrecio() {
		return this.getPrecioDeLista() * 1.10;
	}
	
	public final Double getPrecioDeLista() {
		return this.precioDeLista;
	}
	
	public void setPrecioDeLista(Double precioDeLista) {
		if(precioDeLista > 0) {
			this.precioDeLista = precioDeLista;
		}
	}
}
